"use strict";

var cellauto = {
    "width": 12,
    "speed": 500,
    "current_row": [],
    "max_rows": 20,
    "fifo": [],
};

cellauto.next = function() {  // MOCKUP
    var l = [];
    for (var i = 0; i < cellauto.width; i++) {
        l.push(_.sample([0, 1]));
    }
    return l;
};

cellauto.update_fifo = function() {
    // only calculate new row if there are few rows in the fifo
    if (cellauto.fifo.length <= cellauto.max_rows) {
        cellauto.fifo.push(cellauto.next());
    }
};

cellauto.render = function() {
    // if there is at least one row available to draw
    if (cellauto.fifo.length > 0) {

        // draw it
        var row = $(document.createElement("div"));
        row.addClass("row");
        row.hide(0);

        var data = cellauto.fifo.shift();

        for (var i = 0; i < cellauto.width; i++) {
            var col = $(document.createElement("div"));
            col.addClass("one column");
            col.text(data.shift());
            if (col.text() == "1") {
                col.css({
                    "background-color": "black",
                    "color": "white",
                });
            }
            row.append(col.clone());
        }

        $("#main_container").append(row);
        row.slideDown();

        // check if we want to remove lines from the beginning
        cellauto.cleanup();

        setTimeout(cellauto.render, cellauto.speed);
    }
};

cellauto.cleanup = function() {
    var rows = $(".row").slice(0, $(".rows").length - cellauto.max_rows);
    for (var i = 0; i < rows.length; i++) {
        $(rows[i]).slideUp(function() { $(this).remove(); });
    }
};


// set up fifo
for (var i = 0; i < 10; i++) {
    cellauto.fifo.push(cellauto.next());
}

setInterval(cellauto.update_fifo, cellauto.speed);
cellauto.render();
